<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('api/v1/login', 'Auth\LoginController@verify');


$router->group(['prefix'=>'api/v1', 'middleware' => 'pbe.auth'], function ($router) {
    #to get all data
    $router->get('/songs', 'SongController@getAll');

    #to get data By Id
    $router->get('/songs/{id}', 'SongController@getById');

    #to insert data
    $router->post('/songs', 'SongController@create');

    #to get all data users
    $router->get('/users', 'UserController@getAll');

    #to get data user By Id
    $router->get('/users/{id}', 'UserController@getById');

    #to get data playlist By Id user
    $router->get('/users/{id}/playlists', 'UserController@getplaylistById');

    #untuk mendapatkan lagu dari playlists dengan id tertentu dari user
    $router->get('/users/{id}/playlists/{playlist_id}/songs', 'PlaylistController@getplaylistsongById');



    #to insert data user
    $router->post('/users', 'UserController@create');

    #to insert data
    $router->post('/playlists', 'PlaylistController@createplaylist');

    #to get all data users
    $router->get('/playlists', 'PlaylistController@getAll');


    #untuk mendapatkan detail playlists dengan id tertentu dari user
    $router->get('/playlists/{id}', 'PlaylistController@getdetailplaylistById');

    #to insert data
    $router->post('/playlists/{id}/songs', 'PlaylistSongsController@create');

    #to get all data users
    $router->get('/playlists/{id}/songs', 'PlaylistSongsController@getAll');

    #to get data user By Id
    $router->get('/playlists/{id}/songs', 'PlaylistController@getallplaylistsongById');


    #to update data
//    $router->put('/cashiers/{id}', 'CashierController@update');

    #to delete data
//    $router->delete('/cashiers/{id}', 'CashierController@delete');

//    #single route
//    $router->delete('/cashiers/{id}',[
//       'middleware' => 'pbe.superadmin','uses' => 'CashierController@delete'
//    ]);

    #group route superuser
    $router->group (['middleware' => 'pbe.superuser'], function ($router) {
        $router->delete('/songs/{id}', 'SongController@delete');
        #to update data
        $router->put('/songs/{id}', 'SongController@update');
    });

});
