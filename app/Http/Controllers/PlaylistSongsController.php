<?php

namespace App\Http\Controllers;

use App\Exceptions\PbeNotAuthorizedException;
use App\Model\Playlist;
use App\Model\Playlistsong;
use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaylistSongsController extends Controller
{
    public function songuser()
    {
        $token = request()->header('token');
        $user = User::where('token','=',$token)->first();

        return $user;
    }

    public function getAll()
    {
        /*
         * untuk mendapatkan semua data users
         * @return JsonResponse
         */
        $playlistsongs = Playlistsong::all();
        return $this->successResponse(['playlistsongs' => $playlistsongs]);
    }

    /**
     * function untuk mengambil 1 data dari user berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getById($id)
    {
        $playlistsong = Playlistsong::find($id);
        if ($playlistsong == null){
            throw new NotFoundHttpException();
        }
        return $this->successResponse(['playlistsong' => $playlistsong]);
    }
    /**
     * function untuk menambah data di song
     * @return JsonResponse
     */
    public function create($id)
    {
//        /*validasi*/
//
//        $validate = Validator::make(request()->all(), [
//            'playlist_id' => 'required',
//            'song_id' => 'required',
//        ]);
//        if($validate->fails()){
//            return $this->failResponse($validate->errors()->getMessages(), 400);
//        }
//        /*Jika tidak ada error yang terjadi*/
//        $playlistsong = new Playlistsong();
//        $playlistsong->playlist_id = request('playlist_id');
//        $playlistsong->song_id = request('song_id');
//        $playlistsong->save();
//        return $this->successResponse(['playlistsongs' => $playlistsong], 201);

        $user=$this->songuser();
        $playlist = Playlist::where('user_id','=',$user->id)
            ->where('playlists.id','=',$id)
            ->first();
        if ($playlist == null){
            throw new PbeNotAuthorizedException();
        }
        $playlist = new Playlistsong();
        $playlist->song_id = request('song_id');
        $playlist->playlist_id = $id;
        $playlist->save();
        return $this->successResponse(['playlists' => $playlist], 201);
    }
}
